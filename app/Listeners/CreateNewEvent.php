<?php

namespace App\Listeners;

use App\Events\EventDataIsAvailable;
use App\Repositories\EventRepository;
use App\Repositories\PerformerRepository;
use App\Repositories\PlaceRepository;

class CreateNewEvent
{
    private PlaceRepository $placeRepository;
    private PerformerRepository $performerRepository;
    private EventRepository $eventRepository;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        PlaceRepository $placeRepository,
        PerformerRepository $performerRepository,
        EventRepository $eventRepository
    ) {
        $this->placeRepository = $placeRepository;
        $this->performerRepository = $performerRepository;
        $this->eventRepository = $eventRepository;
    }

    /**
     * Handle the event.
     *
     * @param EventDataIsAvailable $e
     * @return void
     */
    public function handle(EventDataIsAvailable $e): void
    {
        $placeData = $e->data['place'];
        $performersData = $e->data['performers'];

        $eventData = $e->data;
        unset($eventData['place']);
        unset($eventData['performers']);

        $place = $this->placeRepository->firstOrCreate($placeData);
        $event = $this->eventRepository->firstOrCreate($eventData, $place);

        foreach ($performersData as $performerData) {
            $this->performerRepository->firstOrCreate($performerData, $event);
        }
    }
}
