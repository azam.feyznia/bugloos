<?php

namespace App\Clients;

use App\DTOs\EventSearchParams;
use Illuminate\Support\Facades\Facade;

/**
 * @method static getEvent(string $eventCode)
 * @method static searchForEvents(EventSearchParams $params)
 */
class EventsProviderClient extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return EventsProviderManager::class;
    }

}
