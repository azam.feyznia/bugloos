<?php

namespace App\Clients;

use App\DTOs\EventSearchParams;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Manager;

class EventsProviderManager extends Manager
{
    /**
     * @param $driver
     * @return EventsProviderInterface
     */
    public function driver($driver = null): EventsProviderInterface
    {
        $driver = $driver ?? $this->getDefaultDriver();

        if (!isset($this->drivers[$driver])) {
            $this->drivers[$driver] = $this->createDriver($driver);
        }

        return $this->drivers[$driver];
    }

    /**
     * @return string
     */
    public function getDefaultDriver(): string
    {
        return $this->config->get('event-providers.default');
    }

    /**
     * @return EventsProviderInterface
     * @throws BindingResolutionException
     */
    public function createSeatgeekDriver(): EventsProviderInterface
    {
        return app()->make(SeatgeekClient::class);
    }

    /**
     * @param string $eventCode
     * @return array
     */
    public function getEvent(string $eventCode): array
    {
        return $this->driver()->getEvent($eventCode);
    }

    /*
     * @param EventSearchParams $params
     * @return array
     */
    public function searchForEvents(EventSearchParams $params): array
    {
        return $this->driver()->searchForEvents($params);
    }

}
