<?php

namespace App\Clients;

use App\DTOs\EventSearchParams;

interface EventsProviderInterface
{
    /**
     * @param string $eventCode
     * @return array
     */
    public function getEvent(string $eventCode): array;

    /**
     * @param EventSearchParams $params
     * @return array
     */
    public function searchForEvents(EventSearchParams $params): array;
}
