<?php

namespace App\Clients;

use App\DTOs\EventSearchParams;
use App\services\MapperService;
use App\Traits\ClientResponseHelper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class SeatgeekClient extends Client implements EventsProviderInterface
{
    use ClientResponseHelper;

    protected static int $defaultTimeout = 10;
    private MapperService $mapperService;

    /**
     * SeatgeekClient constructor.
     */
    public function __construct(MapperService $mapperService)
    {
        $this->mapperService = $mapperService;

        $options = [
            'base_uri' => $this->getBaseURL(),
            'timeout' => $this->getTimeout(),
        ];

        parent::__construct($options);
    }

    /**
     * @param string $eventCode
     * @return array
     * @throws GuzzleException
     * @throws \Exception
     */
    public function getEvent(string $eventCode): array
    {
        try {
            $response = $this->get('events/' . $eventCode, [
                'query' => $this->getDefaultParams(),
            ]);

            $data = $this->extractResponseData($response);

            return $this->mapEventData($data);
        } catch (\Exception $e) {
            logger()->error($e->getMessage());
            throw new \Exception('SeatgeekClient API error');
        }
    }

    /**
     * @param EventSearchParams $params
     * @return array
     * @throws GuzzleException
     * @throws \Exception
     */
    public function searchForEvents(EventSearchParams $params): array
    {
        try {
            $response = $this->get('events', [
                'query' => $this->getDefaultParams() + $params->toArray(),
            ]);

            $data = $this->extractResponseData($response);
            $data['events'] = collect($data['events'])->map(fn ($event) => $this->mapEventData($event))->toArray();

            return $data;
        } catch (\Exception $e) {
            logger()->error($e->getMessage());
            throw new \Exception('SeatgeekClient API error');
        }
    }

    /**
     * @return string
     */
    private function getBaseURL(): string
    {
        return config('seatgeek.base_url');
    }

    /**
     * @return int
     */
    private function getTimeout(): int
    {
        return config('seatgeek.timeout', static::$defaultTimeout);
    }

    /**
     * @return array
     */
    private function getDefaultParams(): array
    {
        return [
            'client_id' => config('seatgeek.client_id'),
            'client_secret' => config('seatgeek.client_secret'),
            'format' => config('seatgeek.response_format'),
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    private function mapEventData(array $data): array
    {
        $eventData = $this->mapperService->map($data, 'seatgeek' , 'event');
        $eventData['place'] = $this->mapperService->map($eventData['place'], 'seatgeek', 'place');

        foreach ($data['performers'] as $performer) {
            $eventData['performers'][] = $this->mapperService->map($performer, 'seatgeek', 'performer');
        }

        return $eventData;
    }
}
