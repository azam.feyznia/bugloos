<?php

namespace App\services;

use Symfony\Component\Yaml\Yaml;

class MapperService
{
    /**
     * @param array $data
     * @param string $client
     * @param string $class
     * @return array
     */
    public static function map(array $data, string $client, string $class): array
    {
        $mapping = Yaml::parseFile(__DIR__ . '/../../config/'. $client . '-mappings/' . $class . '.yaml');

        return self::mapData($data, $mapping[$class]);
    }

    /**
     * @param array $data
     * @param array $mappings
     * @return array
     */
    private static function mapData(array $data, array $mappings): array
    {
        $mappedData = [];

        foreach ($mappings as $key => $value) {
            if (is_array($value)) {
                $mappedData[$key] = self::mapData($data[$key], $value);
            } else {
                $mappedData[$key] = self::getValue($value, $data);
            }
        }

        return $mappedData;
    }

    /**
     * @param string $value
     * @param array $data
     * @return array|mixed|null
     */
    private static function getValue(string $value, array $data)
    {
        $nestedKeys = explode('.', $value);
        $result = $data;

        foreach ($nestedKeys as $nestedKey) {
            $result = $result[$nestedKey] ?? null;
        }

        return $result;
    }
}
