<?php

namespace App\services;

class XmlFormatterService
{
    /**
     * @param string $xml
     * @return array
     * @throws \Exception
     */
    public static function convertToArray(string $xml): array
    {
        $xmlDom = new \SimpleXmlIterator($xml, null, false);

        return self::xmlToFlattenArray($xmlDom);
    }

    /**
     * @param \SimpleXmlIterator $node
     * @return array
     */
    private static function xmlToFlattenArray(\SimpleXmlIterator $node): array
    {
        $array = [];
        for($node->rewind(); $node->valid(); $node->next() ) {
            if($node->hasChildren()){
                if (isset($array[$node->key()])){
                    $array[] = $array[$node->key()];
                    $array[] = self::xmlToFlattenArray($node->current());
                    unset($array[$node->key()]);
                } else {
                    $array[$node->key()] = self::xmlToFlattenArray($node->current());
                }
            }
            else{
                $array[$node->key()] = strval($node->current());
            }
        }

        return $array;
    }
}
