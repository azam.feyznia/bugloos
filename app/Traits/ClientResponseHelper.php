<?php

namespace App\Traits;

use App\services\XmlFormatterService;
use GuzzleHttp\Psr7\Response;

trait ClientResponseHelper
{
    /**
     * @throws \Exception
     */
    private function extractResponseData(Response $response): array
    {
        $contentType = $this->getContentType($response);

        if ($contentType === 'application/json') {
            return $this->getDataFromJson($response);
        } elseif ($contentType === 'application/xml') {
            return $this->getDataFromXml($response);
        }

        throw new \Exception('Unexpected response type');
    }

    /**
     * @param Response $response
     * @return string
     */
    private function getContentType(Response $response): string
    {
        $contentType = $response->getHeader('Content-Type')[0];

        return explode(';', $contentType)[0];
    }

    /**
     * @param Response $response
     * @return array
     */
    private function getDataFromJson(Response $response): array
    {
        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @throws \Exception
     */
    private function getDataFromXml(Response $response): array
    {
        return XmlFormatterService::convertToArray($response->getBody()->getContents());
    }
}
