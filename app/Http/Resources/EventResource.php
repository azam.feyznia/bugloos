<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array|Arrayable|\JsonSerializable
     */
    public function toArray($request): array|\JsonSerializable|Arrayable
    {
        return [
            'id' => $this->resource->id,
            'code' => $this->resource->code,
            'title' => $this->resource->title,
            'url' => $this->resource->url,
            'start_date' => $this->resource->start_date,
            'topic' => $this->resource->topic,
            'place' => PlaceResource::make($this->resource->place),
            'score' => $this->resource->score,
            'views_count' => $this->resource->views_count,
            'performers' => PerformerResource::collection($this->resource->performers),
            'created_at' => $this->resource->created_at,
            'updated_at' => $this->resource->updated_at,
        ];
    }
}
