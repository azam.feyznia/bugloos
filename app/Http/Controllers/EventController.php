<?php

namespace App\Http\Controllers;

use App\Http\Resources\EventResource;
use App\Repositories\EventRepository;
use Exception;
use Illuminate\Http\JsonResponse;

class EventController extends Controller
{
    private EventRepository $repository;

    /**
     * @param EventRepository $repository
     */
    public function __construct(EventRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $code
     * @return EventResource| JsonResponse
     */
    public function show(string $code): EventResource| JsonResponse
    {
        try {
            $event = $this->repository->getByCode($code);
            $event = $this->repository->increaseViewsCount($event);

            return response()->json([
                'status' => 'success',
                'event' => new EventResource($event),
            ]);

        } catch (Exception $e) {
            logger()->error($e->getMessage(), $e->getTrace());
            return response()->json(['message' => 'Unexpected exception'], 500);
        }
    }
}
