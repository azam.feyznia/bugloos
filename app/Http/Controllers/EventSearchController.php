<?php

namespace App\Http\Controllers;

use App\Clients\EventsProviderClient;
use App\DTOs\EventSearchParams;
use App\Http\Requests\SearchEventRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;

class EventSearchController extends Controller
{
    /**
     * @param SearchEventRequest $request
     * @return JsonResponse
     */
    public function __invoke(SearchEventRequest $request): JsonResponse
    {
        try {
            $params = EventSearchParams::createFromRequest($request);
            $result = EventsProviderClient::searchForEvents($params);

            return response()->json([
                'status' => 'success',
                'events' => collect($result['events'])->map(fn($event) => Arr::only($event, ['title', 'code']))->toArray(),
                'total' => $result['meta']['total'],
            ]);
        } catch (Exception $e) {
            logger()->error($e->getMessage(), $e->getTrace());
            return response()->json(['message' => 'Unexpected exception'], 500);
        }
    }
}
