<?php

namespace App\Repositories;

use App\Models\Event;
use App\Models\Performer;

class PerformerRepository extends Repository
{
    public function firstOrCreate(array $attributes, Event $event): Performer
    {
        $performer = $this->model->firstOrCreate($attributes, $attributes);
        $event->performers()->attach($performer);

        return $performer;
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Performer::class;
    }

}
