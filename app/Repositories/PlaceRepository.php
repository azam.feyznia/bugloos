<?php

namespace App\Repositories;

use App\Models\Place;

class PlaceRepository extends Repository
{

    public function firstOrCreate(array $attributes): Place
    {
        return $this->model->firstOrCreate($attributes, $attributes);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Place::class;
    }
}
