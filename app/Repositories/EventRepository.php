<?php

namespace App\Repositories;

use App\Jobs\GetEventData;
use App\Models\Event;
use App\Models\Place;

class EventRepository extends Repository
{
    /**
     * @param array $attributes
     * @param Place $place
     * @return Event
     */
    public function firstOrCreate(array $attributes, Place $place): Event
    {
        $attributes += ['place_id' => $place->id];

        return $this->model->firstOrCreate($attributes, $attributes);
    }

    /**
     * @param string $code
     * @return Event
     */
    public function getByCode(string $code): Event
    {
        $query = $this->model->newQuery();
        $query = $query->with(['performers', 'place'])->where('code', $code);

        return $query->firstOr(function () use ($code, $query) {
                GetEventData::dispatchSync($code);

                return $query->first();
            }
        );
    }

    /**
     * @param Event $event
     * @return Event
     */
    public function increaseViewsCount(Event $event): Event
    {
        $event->increment('views_count');

        return $event;
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Event::class;
    }
}
