<?php

namespace App\Jobs;

use App\Clients\EventsProviderClient;
use App\Events\EventDataIsAvailable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;

class GetEventData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $code;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $code)
    {
        $this->code = $code;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $response = Cache::remember($this->code, 60 * 60 * 24, fn () => EventsProviderClient::getEvent($this->code));
        EventDataIsAvailable::dispatch($response);
    }

    public function failed()
    {
        logger('Failed to get event data');
    }
}
