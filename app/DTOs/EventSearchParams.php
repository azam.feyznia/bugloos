<?php

namespace App\DTOs;

use Illuminate\Http\Request;

class EventSearchParams
{
    public int $perPage;
    public int $page;
    public string $range;
    public bool $geoip;


    /**
     * @param Request $request
     * @return static
     */
    public static function createFromRequest(Request $request): self
    {
        $instance = new self();
        $instance->page = $request->input('page', 1);
        $instance->perPage = $request->input('per_page', 10);
        $instance->range = $request->input('range', '100mi');
        $instance->geoip = $request->input('geoip', false);

        return $instance;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'page' => $this->page,
            'per_page' => $this->perPage,
            'range' => $this->range,
            'geoip' => $this->geoip,
        ];
    }


}
