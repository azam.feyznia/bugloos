<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'url',
        'start_date',
        'topic',
        'code',
        'place_id',
        'score',
        'views_count',
    ];

    protected $casts = [
        'start_date' => 'datetime',
    ];

    /**
     * @return BelongsTo
     */
    public function place(): BelongsTo
    {
        return $this->belongsTo(Place::class);
    }

    /**
     * @return BelongsToMany
     */
    public function performers(): BelongsToMany
    {
        return $this->belongsToMany(Performer::class);
    }
}
