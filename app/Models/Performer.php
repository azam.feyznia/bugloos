<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Performer extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'url',
        'image',
        'chief',
        'code',
        'type',
        'slug',
    ];

    /**
     * @param $value
     * @return void
     */
    public function setChiefAttribute($value): void
    {
        $this->attributes['chief'] = (bool) $value;
    }

    /**
     * @return BelongsToMany
     */
    public function events(): BelongsToMany
    {
        return $this->belongsToMany(Event::class);
    }
}
