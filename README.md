## About Project

In this project we've implemented an Event Management system. Our events data comes from open source third-party APIs such as [Seatgeek](https://platform.seatgeek.com/). Using this API we can search for events and get event details. We've also implemented a simple database to store some event info. Therefore, We can offer information about event popularity through measuring how often each event was viewed on our system. We've used [laravel](https://laravel.com/) framework for this project.

## Installation

You need to just run this command after pulling source code:
``
composer install
``
