# Changelog
All notable changes to this project will be documented in this file.

## [1.0.2]
### Added
- API endpoint for searching events
- API endpoint for getting event details

## [1.0.1]
### Added
- database migrations and data models

## [1.0.0] - 2023-01-20
- base project
