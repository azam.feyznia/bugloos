<?php

use App\Http\Controllers\EventSearchController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('events')->name('events.')->group(function () {
    Route::get('/', EventSearchController::class)->name('search');
    Route::get('/{code}', [App\Http\Controllers\EventController::class, 'show'])->name('show');
});
