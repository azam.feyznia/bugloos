<?php

namespace Database\Factories;

use App\Models\Place;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Event>
 */
class EventFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => fake()->name(),
            'url' => fake()->url(),
            'start_date' => fake()->dateTime(),
            'topic' => fake()->word(),
            'code' => fake()->word(),
            'place_id' => Place::factory(),
            'score' => fake()->randomFloat(2, 0, 100),
            'views_count' => fake()->randomNumber(),
        ];
    }
}
