<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Place>
 */
class PlaceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'city' => fake()->city(),
            'name' => fake()->name(),
            'country' => fake()->country(),
            'state' => fake()->state(),
            'zip_code' => fake()->postcode(),
            'lat' => fake()->latitude(),
            'lon' => fake()->longitude(),
            'url' => fake()->url(),
            'code' => fake()->word(),
        ];
    }
}
