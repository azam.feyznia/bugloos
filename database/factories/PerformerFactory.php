<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Performer>
 */
class PerformerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'url' => $this->faker->url,
            'chief' => $this->faker->boolean,
            'code' => $this->faker->randomNumber,
            'type' => $this->faker->word,
            'image' => $this->faker->imageUrl,
            'slug' => $this->faker->slug,
        ];
    }
}
