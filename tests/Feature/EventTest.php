<?php

namespace Tests\Feature;

use App\Clients\SeatgeekClient;
use App\Jobs\GetEventData;
use App\Models\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;

class EventTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_returns_event_data()
    {
        $event = Event::factory()->create();

        $response = $this->getJson(route('events.show', $event->code));

        $response->assertStatus(200);
        $response->assertJson([
            'event' => [
                'title' => $event->title,
                'url' => $event->url,
                'topic' => $event->topic,
                'code' => $event->code,
                'place' => [
                    'name' => $event->place->name,
                    'city' => $event->place->city,
                    'country' => $event->place->country,
                    'state' => $event->place->state,
                    'zip_code' => $event->place->zip_code,
                    'lat' => $event->place->lat,
                    'lon' => $event->place->lon,
                    'url' => $event->place->url,
                    'code' => $event->place->code,
                ],
                'score' => strval($event->score),
                'views_count' => $event->views_count + 1,
            ],
        ]);
    }

    /**
     * @test
     */
    public function if_there_was_event_info_in_db_it_didnt_call_api()
    {
        Bus::fake();

        $event = Event::factory()->create();

        $response = $this->getJson(route('events.show', $event->code));

        $response->assertStatus(200);
        Bus::assertNotDispatched(GetEventData::class);
    }

    /**
     * @test
     */
    public function if_there_was_no_event_info_in_db_it_calls_api()
    {
        Bus::fake();

        $this->getJson(route('events.show', '5853526'));
        Bus::assertDispatched(GetEventData::class);
    }

    /**
     * @test
     */
    public function it_stores_event_data_correctly()
    {
        $fakeResponse = $this->getFakeResponse();

        $mock = \Mockery::mock(SeatgeekClient::class)->makePartial();
        $mock->shouldReceive('getEvent')
            ->andReturn($fakeResponse);
        $this->app->instance(SeatgeekClient::class, $mock);

        $response = $this->getJson(route('events.show', '5853526'));

        $response->assertStatus(200);
        $this->assertDatabaseHas('events', [
            'title' => 'Test Event',
            'url' => 'https://www.eventbrite.com/e/test-event-tickets-123456789',
            'start_date' => '2021-01-01 12:00:00',
            'topic' => 'Test Type',
            'code' => '5853526',
        ]);
        $this->assertDatabaseHas('places', [
            'name' => 'Test Venue',
            'city' => 'Test City',
            'state' => 'Test State',
            'zip_code' => '12345',
            'country' => 'Test Country',
            'lat' => '12.345678',
            'lon' => '12.345678',
            'url' => 'https://www.eventbrite.com/e/test-event-tickets-123456789',
            'code' => '456789',
        ]);

        $this->assertDatabaseHas('performers', [
            'name' => 'Test Performer',
            'image' => 'https://www.eventbrite.com/e/test-event-tickets-123456789',
            'type' => 'Test Type',
            'url' => 'https://www.eventbrite.com/e/test-event-tickets-123456789',
            'code' => '12345',
            'slug' => 'test-performer',
            'chief' => true,
        ]);
    }

    private function getFakeResponse(): array
    {
        return [
            'topic' => 'Test Type',
            'code' => 5853526,
            'start_date' => '2021-01-01T12:00:00',
            'place' => [
                'name' => 'Test Venue',
                'city' => 'Test City',
                'state' => 'Test State',
                'zip_code' => '12345',
                'country' => 'Test Country',
                'lat' => '12.345678',
                'lon' => '12.345678',
                'url' => 'https://www.eventbrite.com/e/test-event-tickets-123456789',
                'code' => 456789,
            ],
            'performers' => [
                [
                    'name' => 'Test Performer',
                    'image' => 'https://www.eventbrite.com/e/test-event-tickets-123456789',
                    'type' => 'Test Type',
                    'url' => 'https://www.eventbrite.com/e/test-event-tickets-123456789',
                    'code' => 12345,
                    'chief' => true,
                    'slug' => 'test-performer',
                ],
            ],
            'title' => 'Test Event',
            'url' => 'https://www.eventbrite.com/e/test-event-tickets-123456789',
            'score' => 0.123456789,
        ];
    }
}
