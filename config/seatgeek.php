<?php

return [
    'client_id' => env('SEATGEEK_CLIENT_ID'),
    'client_secret' => env('SEATGEEK_CLIENT_SECRET'),
    'base_url' => env('SEATGEEK_BASE_URL'),
    'response_format' => env('SEATGEEK_RESPONSE_FORMAT', 'json'),
];
